/**
 * @license Copyright 2017 Mintlab B.V.
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission -
 * subsequent versions of the EUPL (the "Licence").
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at: https://joinup.ec.europa.eu/software/page/eupl
 */

const ERROR = 'error';
const OFF = 'off';
const ALWAYS = 'always';
const NEVER = 'never';
const AS_NEEDED = 'as-needed';
const INDENT = 2;
const COMPLEXITY = 3;

module.exports = {
  extends: 'eslint:recommended',
  env: {
    browser: false,
    es6: true,
    node: true,
  },
  rules: {
    'arrow-body-style': [
      ERROR,
      AS_NEEDED,
    ],
    'arrow-parens': [
      ERROR,
      AS_NEEDED,
    ],
    'comma-dangle': [
      ERROR, {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: NEVER,
      },
    ],
    complexity: [
      ERROR,
      COMPLEXITY,
    ],
    'default-case': [
      ERROR,
    ],
    eqeqeq: [
      ERROR,
      'smart',
    ],
    'func-names': [
      ERROR,
      ALWAYS,
    ],
    // NB: while we aim not to turn recommended configuration settings
    // off, this is new in ESLint 5 and not consistent with our
    // existing configuration for function return values.
    'getter-return': [
      OFF,
    ],
    'guard-for-in': [
      ERROR,
    ],
    'id-length': [
      ERROR, {
        min: 2,
        exceptions: [
          'x',
          'y',
        ],
      },
    ],
    indent: [
      ERROR,
      INDENT,
    ],
    'no-alert': [
      ERROR,
    ],
    'no-bitwise': [
      ERROR,
    ],
    'no-caller': [
      ERROR,
    ],
    'no-confusing-arrow': [
      ERROR,
    ],
    'no-console': [
      ERROR, {
        allow: [
          'error',
          'info',
          'trace',
          'warn',
        ],
      },
    ],
    'no-eq-null': [
      ERROR,
    ],
    'no-eval': [
      ERROR,
    ],
    'no-floating-decimal': [
      ERROR,
    ],
    'no-implicit-coercion': [
      ERROR,
    ],
    'no-inline-comments': [
      ERROR,
    ],
    'no-loop-func': [
      ERROR,
    ],
    'no-magic-numbers': [
      ERROR,
    ],
    'no-multi-assign': [
      ERROR,
    ],
    'no-multi-str': [
      ERROR,
    ],
    'no-negated-condition': [
      ERROR,
    ],
    'no-nested-ternary': [
      ERROR,
    ],
    'no-param-reassign': [
      ERROR,
    ],
    'no-proto': [
      ERROR,
    ],
    'no-script-url': [
      ERROR,
    ],
    'no-self-compare': [
      ERROR,
    ],
    'no-sequences': [
      ERROR,
    ],
    'no-shadow': [
      ERROR,
    ],
    'no-underscore-dangle': [
      ERROR,
    ],
    'no-var': [
      ERROR,
    ],
    'no-void': [
      ERROR,
    ],
    'object-shorthand': [
      ERROR,
      ALWAYS, {
        avoidExplicitReturnArrows: true,
        avoidQuotes: true,
      },
    ],
    'one-var': [
      ERROR,
      NEVER,
    ],
    'prefer-rest-params': [
      ERROR,
    ],
    'quote-props': [
      ERROR,
      AS_NEEDED,
    ],
    quotes: [
      ERROR,
      'single',
    ],
    semi: [
      ERROR,
      ALWAYS,
    ],
    strict: [
      ERROR,
      NEVER,
    ],
    'template-curly-spacing': [
      ERROR,
      NEVER,
    ],
    'valid-jsdoc': [
      ERROR,
      {
        requireParamDescription: false,
        requireParamType: false,
        requireReturnDescription: false,
        requireReturnType: true,
        requireReturn: false,
      },
    ],
  },
};
